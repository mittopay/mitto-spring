package com.mitto.repositories;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertNotNull;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import com.mitto.domain.Iou;
import com.mitto.domain.Iou.IouType;
import com.mitto.domain.Iou.Status;
import com.mitto.domain.User;

public class IouRepositoryTest {
	
	@Autowired
	private IouRepository repository;
	
	public void getIou() {
		Iou iou = repository.findOne(1L);
		assertNotNull(iou);
		assertEquals(iou.getId(),1);
	}
	
	@Transactional
	public void getCreator() {
		Iou iou = repository.findOne(1L);
		assertNotNull(iou);
		User creator = iou.getCreator();
		assertNotNull(creator);
		assertEquals(creator.getId(),1);
	}
	
	@Transactional
	public void getInvolved() {
		Iou iou = repository.findOne(1L);
		assertNotNull(iou);
		User involved = iou.getFriendInvolved();
		assertNotNull(involved);
		assertEquals(involved.getId(),2);
	}
	
	public void getStatus() {
		Iou iou = repository.findOne(1L);
		assertNotNull(iou);
		Status status = iou.getStatus();
		assertEquals(Status.SENT,status);
	}

	public void getType() {
		Iou iou = repository.findOne(1L);
		assertNotNull(iou);
		IouType type = iou.getType();
		assertEquals(IouType.SEND,type);
	}
}
