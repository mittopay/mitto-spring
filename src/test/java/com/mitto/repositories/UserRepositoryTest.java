package com.mitto.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import com.mitto.domain.Iou;
import com.mitto.domain.User;

public class UserRepositoryTest {
	
	@Autowired
	UserRepository repository;
	
	private static final String FACEBOOK_ID = "123456";
	
	public void getUserById() {
		User user = repository.findOne(1L);
		assertNotNull(user);
		assertEquals( user.getFacebookId(), FACEBOOK_ID  );
	}
	
	
	@Transactional
	public void getAllInvolved() {
		int expectedSize = 2;
		User user = repository.findOne(2L);
		assertNotNull(user);
		List<Iou> ious = user.getInvolvedIn();
		assertNotNull(ious);
		assertEquals( ious.size(), expectedSize );
	}
	
}