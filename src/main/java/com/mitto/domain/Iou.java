package com.mitto.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="iou")
public class Iou {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;

	@Enumerated(EnumType.ORDINAL)
	@Column(name="transaction_type")
	@JsonProperty("transaction_type")
	private IouType type;
	
	@Column(name="amount")
	@JsonProperty("amount")
	private double amount;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="friend_involved")
	@JsonProperty("friend_involved")
	private User friendInvolved;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="creator")
	@JsonProperty("creator")
	private User creator;
	
	@Column(name="memo")
	@JsonProperty("memo")
	private String memo;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="current_status")
	@JsonProperty("current_status")
	private Status status; 
	
	@Column(name="date_created")
	@JsonProperty("date_created")
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCreated;
	
	@Column(name="date_modified")
	@JsonProperty("date_modified")
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateModified;
	
	public static enum IouType {
		CHARGE,SEND
	}
	
	public static enum Status {
		SENT,APPROVED,DECLINED
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public IouType getType() {
		return type;
	}

	public void setType(IouType type) {
		this.type = type;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public User getFriendInvolved() {
		return friendInvolved;
	}

	public void setFriendInvolved(User friendInvolved) {
		this.friendInvolved = friendInvolved;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
