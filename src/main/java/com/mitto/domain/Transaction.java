package com.mitto.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Transaction {
	
	@JsonProperty("friend")
	private User friend;

	@JsonProperty("amount")
	private double amount;
	
	public User getFriend() {
		return friend;
	}

	public void setFriend(User friend) {
		this.friend = friend;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
}
