package com.mitto.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="user")
public class User implements Serializable {
	
	private static final long serialVersionUID = 3364651537106227283L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="facebook_id",nullable=false)
	@JsonProperty("facebook_id")
	private String facebookId;
	
	@OneToMany(mappedBy="creator")
	@JsonIgnore
	private List<Iou> created;
	
	@OneToMany(mappedBy="friendInvolved")
	@JsonIgnore
	private List<Iou> involvedIn;
	
	@OneToMany(mappedBy="user")
	@JsonIgnore
	private List<Notification> notifications;
	
	@OneToMany(mappedBy="user", cascade=CascadeType.ALL)
	private List<Fcm> fcms;

	@Column(name="facebook_token")
	@JsonIgnore
	private String facebookToken;

	@Column(name="fcm_notification_token")
	private String fcmNotificationToken;

	public String getFcmNotificationToken() {
		return fcmNotificationToken;
	}

	public void setFcmNotificationToken(String fcmNotificationToken) {
		this.fcmNotificationToken = fcmNotificationToken;
	}

	@OneToMany(mappedBy="user", cascade=CascadeType.ALL)
	@JsonIgnore
	private List<MittoToken> mittoTokens;

	@Column(name="display_name")
	@JsonProperty("display_name")
	private String displayName;
	
	public User() {
		mittoTokens = new ArrayList<>();
		fcms = new ArrayList<>();
		notifications = new ArrayList<>();
		involvedIn = new ArrayList<>();
		created = new ArrayList<>();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Iou> getCreated() {
		return created;
	}

	public void setCreated(List<Iou> created) {
		this.created = created;
	}
	
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public List<Iou> getInvolvedIn() {
		return involvedIn;
	}

	public void setInvolvedIn(List<Iou> involvedIn) {
		this.involvedIn = involvedIn;
	}

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public List<Fcm> getFcms() {
		return fcms;
	}

	public void setFcms(List<Fcm> fcms) {
		this.fcms = fcms;
	}

	public List<MittoToken> getMittoTokens() {
		return mittoTokens;
	}

	public String getFacebookToken() {
		return facebookToken;
	}

	public void setFacebookToken(String facebookToken) {
		this.facebookToken = facebookToken;
	}

	public void setMittoTokens(List<MittoToken> mittoTokens) {
		this.mittoTokens = mittoTokens;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if( !(obj instanceof User) ) return false;
		User user = (User) obj;
		if( user.getId() != this.getId() ) return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(id,displayName,facebookId);
	}
	
}
