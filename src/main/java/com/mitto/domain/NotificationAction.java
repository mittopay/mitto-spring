package com.mitto.domain;

import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mitto.domain.Action.ActivityType;
import com.mitto.domain.Action.OperationType;

public class NotificationAction {

	@JsonProperty("id")
	private long id;
	@JsonProperty("actor")
    private User actor;
	@JsonProperty("activity_type")
    private ActivityType activityType;
	@JsonProperty("operation_type")
    private OperationType operationType;
	@JsonProperty("date_created")
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	private Date dateCreated;
	@JsonProperty("date_modified")
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	private Date dateModified;
	
	@JsonProperty("seen")
	private boolean seen;

	@JsonProperty("iou")
	private Iou iou;
	
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public boolean isSeen() {
		return seen;
	}

	public void setSeen(boolean seen) {
		this.seen = seen;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public User getActor() {
        return actor;
    }
	
    public Iou getIou() {
		return iou;
	}

	public void setIou(Iou iou) {
		this.iou = iou;
	}

	public void setActor(User actor) {
        this.actor = actor;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }
}
