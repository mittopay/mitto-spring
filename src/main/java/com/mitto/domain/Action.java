package com.mitto.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**  
 * @author Scott
 */
@Entity
@Table(name="action")
public class Action {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonProperty("id")
	private long id;
	
	/**
	 * The user that causes the event.
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="actor")
	@JsonProperty("actor")
	private User actor;
	
	/**
	 * The unique identifier of the row that
	 * created this notification.
	 */
	@Column(name="parent_id")
	@JsonProperty("parentId")
	private long parentId;
	
	/**
	* The entity type that spawned this notification ie;
	* (User,FCM,Iou) 
	*/
	@Enumerated(EnumType.ORDINAL)
	@Column(name="parent_type")
	@JsonProperty("activityType")
	private ActivityType activityType;
	
	@Column(name="action_type")
	@JsonProperty("operationType")
	private OperationType operationType;

	/**
	* Time the notification was created
	*/
	@Column(name="created_at")
	@JsonProperty("createdAt")
	private Date createdAt;
	
	@OneToMany(mappedBy="action")
	@JsonIgnore
	private List<Notification> notifications;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getActor() {
		return actor;
	}

	public void setActor(User actor) {
		this.actor = actor;
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public ActivityType getActivityType() {
		return activityType;
	}

	public void setActivityType(ActivityType activityType) {
		this.activityType = activityType;
	}

	public OperationType getOperationType() {
		return operationType;
	}

	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public enum ActivityType {
		USER,IOU,FCM
	}
	
	public enum OperationType {
		NEW,UPDATED,ACCEPT,DECLINE
	}

	public static Action fcmAction(Fcm fcm, OperationType opType ) {
		Action action = new Action();
		action.setActivityType(ActivityType.FCM);
		action.setOperationType(opType);
		action.setActor(fcm.getUser());
		action.setCreatedAt(new Date());
		action.setParentId(fcm.getId());
		return action;
	}
	
	public static Action iouAction( Iou iou, OperationType opType  ) {
		Action action = new Action();
		action.setActivityType(ActivityType.IOU);
		action.setOperationType(opType);
		action.setActor(iou.getCreator());
		action.setCreatedAt( new Date() );
		action.setParentId(iou.getId());
		return action;
	}
	
	public static Action userAction( User user, OperationType opType ) {
		Action action = new Action();
		action.setActivityType(ActivityType.USER);
		action.setOperationType(opType);
		action.setActor(user);
		action.setCreatedAt( new Date() );
		action.setParentId(user.getId());
		return action;
	}
}
