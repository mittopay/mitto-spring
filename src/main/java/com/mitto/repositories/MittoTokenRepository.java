package com.mitto.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mitto.domain.MittoToken;

@Repository
@Transactional
public interface MittoTokenRepository extends PagingAndSortingRepository<MittoToken, Long>{
	MittoToken findByToken( String token );
}
