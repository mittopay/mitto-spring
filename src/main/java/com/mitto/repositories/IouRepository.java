package com.mitto.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mitto.domain.Iou;
import com.mitto.domain.User;

@Repository
public interface IouRepository extends PagingAndSortingRepository<Iou, Long>{
	
	public List<Iou> findAllByCreatorOrFriendInvolved( User creator, User friendInvolved );

	@Query("select sum(amount) from Iou iou WHERE " + 
	       " ( iou.creator = :user  AND iou.type = 0) " +
		   " OR ( iou.friendInvolved = :user and type = 1) ")
	public double getAmountOwed( @Param("user") User user);

	@Query("select sum(amount) from Iou iou WHERE " + 
	       " ( iou.creator = :user AND iou.type = 1) " +
		   " OR ( iou.friendInvolved = :user and type = 0) ")
	public double getAmountLent( @Param("user") User user);
	
	@Query("select iou from Iou iou WHERE " + 
	       " ( iou.creator = :user and friendInvolved = :friend ) " +
		   " OR ( iou.friendInvolved = :user and iou.creator = :friend ) ")
	public List<Iou> findAllThatInvolve( @Param("user") User user, @Param("friend") User friend );
}
