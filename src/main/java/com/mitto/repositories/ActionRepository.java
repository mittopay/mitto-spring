package com.mitto.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mitto.domain.Action;

@Repository
public interface ActionRepository extends PagingAndSortingRepository<Action, Long>{


}
