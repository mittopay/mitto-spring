package com.mitto.repositories;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mitto.domain.Notification;
import com.mitto.domain.User;

@Repository
public interface NotificationRepository extends PagingAndSortingRepository<Notification, Long>{
	
	@Query("SELECT n FROM Notification n INNER JOIN n.action a WHERE n.user = :user ORDER BY a.createdAt DESC")
	@Fetch(FetchMode.SELECT)
	Page<Notification> findAllOrderByCreatedAtEagerly(Pageable p, @Param("user") User user );
}
