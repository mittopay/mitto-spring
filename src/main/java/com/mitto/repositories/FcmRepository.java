package com.mitto.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mitto.domain.Fcm;

@Repository
public interface FcmRepository extends PagingAndSortingRepository<Fcm, Long>{

}
