package com.mitto.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mitto.domain.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long>{
	User findByFacebookId( String facebookId );

	@Query("select u from User u inner join u.mittoTokens t where t.token = :token  ")
	User findByToken(@Param("token") String token);

}