package com.mitto.config;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import com.mitto.security.ExtractAuthTokenFilter;
import com.mitto.security.MittoAuthenticationProvider;
import com.mitto.service.user.UserService;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired UserService userService;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider( provideAuthenticationProvider(userService));
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.
			authorizeRequests()
				.antMatchers("/users/login").permitAll()
				.antMatchers("/v2/api-docs").permitAll()
				.antMatchers("/**").authenticated()
				.and()
				.csrf().disable();
		http.addFilterBefore(provideFilter(), BasicAuthenticationFilter.class);
	} 
	
	@Bean
	public AuthenticationEntryPoint unauthorizedEntryPoint() {
		return (request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
	}

	ExtractAuthTokenFilter provideFilter() {
		return new ExtractAuthTokenFilter();
	}	
	
	@Bean
	AuthenticationProvider provideAuthenticationProvider( UserService userService ) {
		return new MittoAuthenticationProvider(userService);
	}
}
