package com.mitto.web;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.mitto.domain.MittoToken;
import com.mitto.domain.User;
import com.mitto.domain.request.LoginRequest;
import com.mitto.domain.response.LoginResponse;
import com.mitto.service.user.FacebookUserService;
import com.mitto.service.user.UserService;

@RestController
@RequestMapping("users")
public class UserController {
	
	private UserService service;
	
	@Autowired
	public UserController( FacebookUserService service ) {
		this.service = service;
	}
	
	/**
	 * On successful login, provides a user with their user details and their authentication token.
	 * @param loginRequest Contains the users id and password (facebook token)
	 * @return The users details and their token.
	 */
	@RequestMapping( value = "/login", method = RequestMethod.POST ) 
	public LoginResponse login( @RequestBody LoginRequest loginRequest ) {
		
		User user = new User();
		user.setFacebookId( loginRequest.getFacebookId() );
		user.setFacebookToken(loginRequest.getFacebookToken());
		MittoToken token = service.login(user);
		
		LoginResponse response = new LoginResponse();
		response.setToken(token.toString());
		response.setUser(user);

		return response;
	}
	
	/**
	 * Retrieves a specified user.
	 * @param id The user to look up.
	 * @return The users details.
	 */
	@RequestMapping( value = "/{id}", method = RequestMethod.GET )
	public User get( @PathVariable("id") long id ) {
		if( id <= 0 ) throw new BadRequestException("Id was not valid.");
		User user = service.getUser(id);
		if( user == null ) throw new NotFoundException("User was not found");
		return user;
	}
	
	
	
}