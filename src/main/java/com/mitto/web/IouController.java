package com.mitto.web;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.mitto.domain.Iou;
import com.mitto.domain.Transaction;
import com.mitto.domain.User;
import com.mitto.service.iou.IouService;

@RestController
@RequestMapping("ious")
public class IouController {
	
	private Logger logger = LoggerFactory.getLogger(UserController.class);
	private IouService service;
	
	@Autowired
	public IouController( IouService service ) {
		this.service = service;
	}
	
	@RequestMapping( method = RequestMethod.POST ) 
	public Iou create( @RequestBody Iou iou ) {
		logger.debug("Restful - Create was called");
		return service.create(iou);
	}
	
	@RequestMapping( value = "/{id}/accept", method = RequestMethod.PUT ) 
	public void accept( @PathVariable("id") long id ) {
		service.accept(id);
	}
	
	@RequestMapping( value = "/{id}/decline", method = RequestMethod.PUT ) 
	public void decline( @PathVariable("id") long id ) {
		service.decline(id);
	}

	@RequestMapping( value = "/created", method = RequestMethod.GET ) 
	public List<Iou> getCreated( @AuthenticationPrincipal User user ) {
		return service.getAllCreated(user);
	}
	
	@RequestMapping( value = "/involved", method = RequestMethod.GET ) 
	public List<Iou> getInvolved( @AuthenticationPrincipal User user ) {
		return service.getAllInvolved(user);
	}
	
	@RequestMapping( value = "/friends", method = RequestMethod.GET ) 
	public List<Transaction> getTransactions( @AuthenticationPrincipal User user ) {
		List<Transaction> transactions = service.getAllTransactions( user );
		return transactions;
	}

	@RequestMapping( value = "/owing", method = RequestMethod.GET ) 
	public double getOwing( @AuthenticationPrincipal User user) {
		return service.getAmountOwing(user);
	}

	@RequestMapping( value = "/lent", method = RequestMethod.GET )
	public double getLent( @AuthenticationPrincipal User user) {
		return service.getAmountOwed(user);
	}
	
	@RequestMapping( value = "/friend/{id}", method = RequestMethod.GET )
	public List<Iou> getAllThatInvolve( @AuthenticationPrincipal User user, @PathVariable("id") long friendId ) {
		return service.getAllThatInvolve(user, friendId);
	}
}