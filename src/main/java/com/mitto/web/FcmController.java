package com.mitto.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.mitto.domain.Fcm;
import com.mitto.domain.User;
import com.mitto.service.fcm.FcmService;

@RestController
@RequestMapping("fcms")
public class FcmController {
	
	private FcmService service;
	
	@Autowired
	public FcmController( FcmService service ) {
		this.service = service;
	}
	
	@RequestMapping( method = RequestMethod.POST ) 
	public Fcm create( @AuthenticationPrincipal User user, @RequestBody Fcm fcm ) {
		return service.create( user, fcm.getToken() );
	}
}
