package com.mitto.web;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.mitto.domain.NotificationAction;
import com.mitto.domain.User;
import com.mitto.service.notification.NotificationService;

@RestController
@RequestMapping("notifications")
public class NotificationController {
	
	private NotificationService service;
	
	@Autowired
	public NotificationController( NotificationService service ) {
		this.service = service;
	}
	
	/**
	 * Retrieves all Notifications ordered by time.
	 * @param user The user associated to each notification.
	 * @param pageable The pageable index.
	 * @return A list of notifications ordered by created.
	 */
	@RequestMapping( value = "/latest", method = RequestMethod.GET ) 
	public List<NotificationAction> getLatest( @AuthenticationPrincipal User user, Pageable pageable ) {
		return service.getLatest( user, pageable);
	}
	
	@RequestMapping( value = "/{id}/read", method = RequestMethod.PUT ) 
	public void makeAsRead( @PathVariable("id") int notificationId ) {
		service.makeAsRead(notificationId);
	}
	
}
