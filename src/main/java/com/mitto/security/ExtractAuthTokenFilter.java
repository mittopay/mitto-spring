package com.mitto.security;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

public class ExtractAuthTokenFilter extends GenericFilterBean {
	
	private static final String AUTH_HEADER = "Authorization";
	private Logger logger = LoggerFactory.getLogger(ExtractAuthTokenFilter.class);

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest)request;
		String token = httpRequest.getHeader(AUTH_HEADER);
		
		if( token != null && !token.isEmpty() ) {
			Authentication auth = new AuthenticationToken(token, null, new ArrayList<>());
			logger.debug("Found token, adding to security context");
			SecurityContextHolder.getContext().setAuthentication(auth);
		} else {
			logger.debug("Token was not found");
		}
		chain.doFilter(request, response);
	}
}
