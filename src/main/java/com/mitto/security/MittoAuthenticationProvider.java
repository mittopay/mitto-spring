package com.mitto.security;

import java.util.ArrayList;

import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.mitto.domain.User;
import com.mitto.service.user.UserService;

public class MittoAuthenticationProvider implements AuthenticationProvider {
	
	private UserService userService;
	
	public MittoAuthenticationProvider( UserService userService ) {
		this.userService = userService;
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		if( authentication == null && ! (authentication instanceof AuthenticationToken) ) 
			throw new AuthenticationCredentialsNotFoundException("No authentication provided");
		
		String token = (String)authentication.getCredentials();
		
		if( token == null || token.isEmpty() )
			throw new AuthenticationCredentialsNotFoundException("No authentication provided");

		User user = userService.getUser(token);
		if( user == null ) throw new AuthenticationCredentialsNotFoundException("Bad token provided");
		
		ArrayList<GrantedAuthority> grantedAuthorites = new ArrayList<>();
		grantedAuthorites.add( new SimpleGrantedAuthority("USER") );
		Authentication auth = new AuthenticationToken(token, user, grantedAuthorites );
		auth.setAuthenticated(true);
		SecurityContextHolder.getContext().setAuthentication(auth);
		return auth;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return AuthenticationToken.class.isAssignableFrom(authentication);
	}
}
