package com.mitto.security;

import java.util.Collection;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import com.mitto.domain.User;

public class AuthenticationToken extends AbstractAuthenticationToken {
	
	private static final long serialVersionUID = -654571093299958930L;
	private User user;
	private String token;

	public AuthenticationToken( String token, User user, Collection<? extends GrantedAuthority> authorities) {
		super(authorities);
		this.token = token;
		this.user = user;
	}
	
	@Override
	public Object getCredentials() {
		return this.token;
	}

	@Override
	public Object getPrincipal() {
		return this.user;
	}
}
