package com.mitto.service.fcm;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mitto.domain.Action;
import com.mitto.domain.Fcm;
import com.mitto.domain.User;
import com.mitto.domain.Action.OperationType;
import com.mitto.repositories.FcmRepository;
import com.mitto.service.action.ActionService;

@Service
public class FcmService {
	
	public FcmRepository repository;
	public ActionService actionService;
	
	@Autowired
	public FcmService( FcmRepository repository, ActionService actionService ) {
		this.repository = repository;
		this.actionService = actionService;
	}

	@Transactional
	public Fcm create( User user, String token ) {
		if( token == null || token.isEmpty() ) throw new IllegalArgumentException("Token not provided");
		Fcm fcm = new Fcm();
		fcm.setToken(token);
		fcm.setUser( user );
		fcm = repository.save(fcm);
		
		// Create and process action
		Action action = Action.fcmAction(fcm,OperationType.NEW);
		action = actionService.create( action );

		try {
			actionService.processAction(action);
		} catch (IOException | TimeoutException e) {
			e.printStackTrace();
		}

		return fcm;
	}
}
