package com.mitto.service.action;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mitto.domain.Action;
import com.mitto.repositories.ActionRepository;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

@Service
public class MittoActionService implements ActionService {
	
	private ActionRepository repository;
	
	@Autowired
	public MittoActionService( ActionRepository repository ) {
		this.repository = repository;
	}
	
	@Override
	public Action create(Action action) {
		if( action == null ) throw new IllegalArgumentException("Action not provided");
		return repository.save(action);
	}

	@Override
	public void delete(Action action) {
		if( action == null ) throw new IllegalArgumentException("Action not provided");
		repository.delete(action);
	}

	@Override
	public Action getAction(long id) {
		if( id <= 0 ) throw new IllegalArgumentException("Bad id provided");
		return repository.findOne(id);
	}
	
	@Override
	@Transactional
	public void processAction( Action action ) throws IOException, TimeoutException {
		String queueName = "mittomind";
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.queueDeclare(queueName, false, false, false, null);
		String message = String.valueOf( action.getId() );
		channel.basicPublish("", queueName, null, message.getBytes());
		channel.close();
	}
	
	
}
