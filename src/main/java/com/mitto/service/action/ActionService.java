package com.mitto.service.action;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import com.mitto.domain.Action;

public interface ActionService {

	/**
	 * Create an action.
	 * @param action The action to be created.
	 * @return The created action.
	 */
	Action create( Action action );
	
	/**
	 * Delete an action
	 * @param action The action to be deleted.
	 */
	void delete( Action action );
	
	/**
	 * Get an action.
	 * @param id The action id.
	 * @return The action with above id.
	 */
	Action getAction( long id );
	
	
	/**
	* Sends an action to mittomind which will process it further. 
	*/
	void processAction( Action action )throws IOException, TimeoutException;
	
	
}
