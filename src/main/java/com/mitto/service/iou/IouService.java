package com.mitto.service.iou;

import java.util.List;

import com.mitto.domain.Iou;
import com.mitto.domain.Transaction;
import com.mitto.domain.User;

public interface IouService {
	
	/**
	 * Creates an Iou.
	 * @param The iou to be created.  
	 * @return The created Iou
	 */
	Iou create( Iou iou );
	
	/**
	 * Accepts an Iou 
	 * @param iou The iou to accept.
	 */
	void accept( long id );
	
	/**
	 * Declined an Iou 
	 * @param iou The iou to decline.
	 */
	void decline( long id );
	

	/**
	 * Get a list of all ious involved to this user. 
	 * @param userId The involved user.
	 * @return A list of all ious.
	 */
	List<Iou> getAllInvolved( User user );
	
	List<Iou> getAllCreated( User user );
	
	/**
	 * Get an Iou 
	 * @param id
	 * @return The iou.
	 */
	Iou get( long id );

	List<Transaction> getAllTransactions(User user);

	public double getAmountOwing( User user );
	
	public double getAmountOwed( User user );
	
	public Transaction getTransaction( User user, User friend );
	
	public List<Iou> getAllThatInvolve( User user, long friendId );

}
