package com.mitto.service.iou;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeoutException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mitto.domain.Action;
import com.mitto.domain.Action.OperationType;
import com.mitto.domain.Iou;
import com.mitto.domain.Iou.IouType;
import com.mitto.domain.Iou.Status;
import com.mitto.domain.Transaction;
import com.mitto.domain.User;
import com.mitto.repositories.IouRepository;
import com.mitto.repositories.UserRepository;
import com.mitto.service.action.ActionService;

@Service
public class MittoIouService implements IouService {
	
	private IouRepository repository;
	private UserRepository userRepository;
	private ActionService actionService;

	@PersistenceContext
    private EntityManager manager;
	
	@Autowired
	public MittoIouService( IouRepository repository, UserRepository userRepository, ActionService actionService ) {
		this.repository = repository;
		this.actionService = actionService;
		this.userRepository = userRepository;
	}

	@Override
	@Transactional
	public Iou create(Iou iou) {
		if( iou == null ) throw new IllegalArgumentException("Iou not provided");
		
		// Load creator and friend by facebook id.
		User creator = userRepository.findByFacebookId(iou.getCreator().getFacebookId());
		User friendInvolved = userRepository.findByFacebookId(iou.getFriendInvolved().getFacebookId());
		
		iou.setCreator(creator);
		iou.setFriendInvolved(friendInvolved);
		iou.setStatus( Status.SENT );

		// Save dates
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		Date now = new Date();
		iou.setDateCreated( now );
		iou.setDateModified( now );

		Iou savedIou = repository.save(iou);
		createAndSendAction(savedIou, OperationType.NEW );
		return savedIou;
	}
	
	private void createAndSendAction( Iou iou, OperationType opType ) {
		Action acceptAction = Action.iouAction(iou, opType );
		actionService.create(acceptAction);

		try {
			actionService.processAction(acceptAction);
		} catch (IOException | TimeoutException e) {
			e.printStackTrace();
			throw new IllegalStateException("Could not process accepting iou",e);
		}
	}

	@Override
	@Transactional
	public void accept( long id ) {
		if( id <= 0 ) throw new IllegalArgumentException("Iou Id not provided");
		Iou iouToBeAccepted = repository.findOne(id);
		
		if( iouToBeAccepted == null ) {
			throw new NotFoundException("Iou does not exist.");
		}

		if( iouToBeAccepted.getStatus() != Status.SENT ) {
			throw new IllegalStateException("Iou has already been evaluated.");
		}

		iouToBeAccepted.setStatus(Status.APPROVED);
		iouToBeAccepted.setDateModified( new Date() );
		repository.save(iouToBeAccepted);
		createAndSendAction(iouToBeAccepted, OperationType.ACCEPT );
	}

	@Override
	@Transactional
	public void decline( long id ) {
		if( id <= 0 ) throw new IllegalArgumentException("Iou Id not provided");
		Iou iouToBeDeclined = repository.findOne( id );

		if( iouToBeDeclined == null ) {
			throw new NotFoundException("Iou does not exist.");
		}
		
		if( iouToBeDeclined.getStatus() != Status.SENT ) {
			throw new IllegalStateException("Iou has already been evaluated.");
		}
		
		iouToBeDeclined.setStatus(Status.DECLINED);
		iouToBeDeclined.setDateModified( new Date() );
		repository.save(iouToBeDeclined);
		createAndSendAction(iouToBeDeclined, OperationType.DECLINE );
	}

	@Override
	public List<Iou> getAllInvolved( User user ) {
		if( user == null ) throw new IllegalArgumentException("User not provided");
		return user.getInvolvedIn();
	}
	
	@Override
	public Iou get(long id) {
		if( id <= 0 ) throw new IllegalArgumentException("Iou Id not provided");
		Iou iou = repository.findOne(id);
		if( iou == null ) throw new NotFoundException("Iou does not exist.");
		return iou;
	}

	@Override
	public List<Iou> getAllCreated(User user) {
		if( user == null ) throw new IllegalArgumentException("User not provided");
		return user.getCreated();
	}


	/**
	 * Return a list of transactions where each transaction represents the total oweing to a friend. 
	 */
	@Override
	@Transactional
	public List<Transaction> getAllTransactions(User user) {
		
		List<Transaction> transactions = new ArrayList<>();

		String query=
				"select sum( owed_to_you ) as owed_per_friend, friend " +
				"from " +
				"(select sum(case when iou.transaction_type = 0 then iou.amount else -iou.amount end) AS owed_to_you, " +
			        "iou.friend_involved AS friend " +
			    "FROM iou where creator = ? and iou.current_status = 1 group by friend " +

				"union all " +

				"select sum(case when (iou.transaction_type = 0) then -iou.amount else iou.amount end) AS owed_to_you, " +
			        "iou.creator AS friend " +
			    "FROM iou where friend_involved = ? and iou.current_status = 1 group by friend) as v group by friend";

		Query q = manager.createNativeQuery(query);
		q.setParameter(1, user.getId() );
		q.setParameter(2, user.getId() );
		
		List<Object[]> results = q.getResultList();
			
		results.stream().forEach( record -> {
			double amount = ((Double) record[0]).doubleValue();
			long friendId = ((Integer) record[1]).longValue();
			
			User friend = userRepository.findOne(friendId);
			
			// Create transaction
			Transaction transaction = new Transaction();
			transaction.setAmount(amount);
			transaction.setFriend(friend);
			
			transactions.add(transaction);
		});

		return transactions;
	}
	
	
	/**
	 * Checks to see if the given user is owed money.
	 * @param user The user 
	 * @return True if the given users owes money.
	 */
	public boolean userIsOwedMoney( Iou iou, User user ) {
		if( user.equals(iou.getCreator()) ) {
			return iou.getType() == IouType.CHARGE ? true : false;
		} else if( user.equals(iou.getFriendInvolved()) ){
			return iou.getType() == IouType.CHARGE ? false: true;
		} 
		return false;
	}		
	
	@Override
	public double getAmountOwing( User user ) {
		String query=" select sum(case when (owed_per_friend > 0) then owed_per_friend else 0 end) "
				+ "from ( " +
				"select sum( owed_to_you ) as owed_per_friend " +
				"from " +
				"(select sum(case when iou.transaction_type = 0 then -iou.amount else iou.amount end) AS owed_to_you, " +
			        "iou.friend_involved AS friend " +
			    "FROM iou where creator = ? and iou.current_status = 1 group by friend " +

				"union all " +

				"select sum(case when (iou.transaction_type = 0) then iou.amount else -iou.amount end) AS owed_to_you, " +
			        "iou.creator AS friend " +
			    "FROM iou where friend_involved = ? and iou.current_status = 1 group by friend) as v group by friend ) as t";

		Query q = manager.createNativeQuery(query);
		q.setParameter(1, user.getId() );
		q.setParameter(2, user.getId() );
		Double amountLent = (Double) q.getSingleResult();
		return Math.abs( amountLent != null ? amountLent : 0 );
	}
	
	@Override
	public double getAmountOwed( User user ) {
		
		String query=" select sum(case when (owed_per_friend > 0) then owed_per_friend else 0 end) "
				+ "from ( " +

				"select sum(owed_to_you) as owed_per_friend " +
				"from " +

				"(select sum( case when iou.transaction_type = 0 then iou.amount else -iou.amount end) AS owed_to_you, " +
			        "iou.friend_involved AS friend " +
			    "FROM iou where creator = ? and iou.current_status = 1 group by friend " +

				"union all " +

				"select sum(case when (iou.transaction_type = 0) then -iou.amount else iou.amount end) AS owed_to_you, " +
			        "iou.creator AS friend " +
			    "FROM iou where friend_involved = ? and iou.current_status = 1 group by friend) as v group by friend ) as t";

		Query q = manager.createNativeQuery(query);
		q.setParameter(1, user.getId() );
		q.setParameter(2, user.getId() );
		Double amountLent = (Double) q.getSingleResult();
		return amountLent != null ? amountLent : 0;
	}

	/**
	 * Gets the transaction of a user-to-user relationship.
	 */
	public Transaction getTransaction( User user, User friend ) {

		String query="select sum( owed_to_you ) " +
				"from " +

				"(select sum( case when (iou.transaction_type = 0) then -iou.amount else iou.amount end) AS owed_to_you, " +
			        "iou.friend_involved AS friend " +
			    "FROM iou where creator = ? and friend_involved = ? and current_status = 1 group by friend " +

				"union all " +

				"select sum(case when (iou.transaction_type = 0) then iou.amount else -iou.amount end) AS owed_to_you, " +
			        "iou.friend_involved AS friend " +
			    "FROM iou where friend_involved = ? and creator = ? and current_status = 1 group by friend) as v ";
		Query q = manager.createNativeQuery(query);
		q.setParameter(1, user.getId() );
		q.setParameter(2, friend.getId() );
		q.setParameter(3, user.getId() );
		q.setParameter(4, friend.getId() );
		Double amount = (Double) q.getSingleResult();
		
		
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setFriend(friend);
		return transaction;
	}
	
	public List<Iou> getAllThatInvolve( User user, long friendId ) {
		User friend = userRepository.findOne(friendId);
		return repository.findAllThatInvolve(user, friend);
	}
}
