package com.mitto.service.notification;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mitto.domain.Action;
import com.mitto.domain.Iou;
import com.mitto.domain.Notification;
import com.mitto.domain.Notification.Status;
import com.mitto.domain.NotificationAction;
import com.mitto.domain.User;
import com.mitto.repositories.IouRepository;
import com.mitto.repositories.NotificationRepository;

@Service
public class NotificationService {
	
	private NotificationRepository repository;
	private IouRepository IouRepository;
	
	@Autowired
	public NotificationService( NotificationRepository repository, IouRepository iouRepository ) {
		this.repository = repository;
		this.IouRepository = iouRepository;
	}
	
	/**
	 * @param user
	 * @return
	 */
	public List<Notification> getAllUnread( User user ) {
		return null;
	}
	
	/**
	 * Provides a list of notifications ordered by time.
	 * @param user the user associated to each notification.
	 * @param pageable the pageable index.
	 * @return a list of notifications ordered by created.
	 */
	@Transactional
	public List<NotificationAction> getLatest(User user, Pageable pageable) {
		Page<Notification> page = repository.findAllOrderByCreatedAtEagerly(pageable, user);
		return page.getContent().stream()
			.map( n -> {
				Action action = n.getAction();
				NotificationAction notificationAction = new NotificationAction();
				notificationAction.setActivityType( action.getActivityType() );
				notificationAction.setId( n.getId() );
				notificationAction.setOperationType( action.getOperationType() );
				notificationAction.setActor( action.getActor() );
				notificationAction.setSeen( n.getStatus() == Status.READ );
				notificationAction.setDateCreated(n.getDateCreated());
				notificationAction.setDateModified(n.getDateModified());

				switch( action.getActivityType() ) {
					case USER:
						break;
					case FCM:
						break;
					case IOU:
						Iou iou = IouRepository.findOne(action.getParentId());
						notificationAction.setIou( iou );
						break;
				}
				return notificationAction;
		}).collect( Collectors.toList() );
	}
	
	@Transactional
	public void makeAsRead( long notificationId ) {
		Notification notification = repository.findOne(notificationId); 
		notification.setStatus( Status.READ );
		repository.save(notification);
	}
}
