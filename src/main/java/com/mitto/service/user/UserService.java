package com.mitto.service.user;

import org.springframework.transaction.annotation.Transactional;

import com.mitto.domain.MittoToken;
import com.mitto.domain.User;

public interface UserService {

	/**
	 * Performs a login for the user, if the user does not already
	 * exist, the user will be created.
	 * @param user The user to be created or signed in.
	 * @return The authentication token.
	 */
	@Transactional
	MittoToken login(User user);

	@Transactional
	User getUser( String authToken );
	
	@Transactional
	User getUser( long id );
	
	
}
