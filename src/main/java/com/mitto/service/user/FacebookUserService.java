package com.mitto.service.user;

import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mitto.domain.MittoToken;
import com.mitto.domain.User;
import com.mitto.repositories.UserRepository;

@Service
public class FacebookUserService implements UserService {
	
	private UserRepository repository;
	
	@Autowired
	public FacebookUserService( UserRepository repository ) {
		this.repository = repository;
	}

	@Transactional
	public User getUser( String authToken ) {
		if( authToken == null || authToken.isEmpty() ) return null;
		User user = repository.findByToken(authToken);
		return user;
	}
	

	/**
	 * Returns a user looked up from the provided user id. Returns null
	 * if the user does not exist.
	 * @param id The id to lookup the user.
	 * @return User the found user.
	 * @throws IllegalArgumentException if the id is not valid.
	 */
	@Transactional
	public User getUser( long id ) {
		if( id <= 0 ) throw new IllegalArgumentException("Id is not valid");
		return repository.findOne(id);
	}

	private User createFacebookUser(String facebookId, String facebookToken) {

		Facebook facebook = new FacebookTemplate(facebookToken);
		String displayName = facebook.userOperations().getUserProfile().getName();

		// Create user
		User user = new User();
		user.setFacebookId(facebookId);
		user.setFacebookToken(facebookToken);
		user.setDisplayName(displayName);
		return repository.save(user);
	}
	
	@Override
	/**
	 * Performs a login for the user, if the user does not already
	 * exist, the user will be created.
	 * @param user The user to be created or signed in.
	 * @return The authentication token.
	 */
	@Transactional
	public MittoToken login( User user ) {
		
		if( user == null ) throw new IllegalArgumentException("User not provided");
		
		// Get id and token
		String facebookId = user.getFacebookId();
		String facebookToken = user.getFacebookToken();

		if( facebookToken == null || facebookToken.isEmpty() ) throw new IllegalArgumentException("Facebook token not provided"); 
		if( facebookId == null || facebookId.isEmpty() ) throw new IllegalArgumentException("Facebook id not provided");

		// Check if the facebook_id is valid and exists.
		if( !facebookUserExists( user, facebookToken ) ) throw new BadCredentialsException("facebook user does not exist");
		
		// Get user, if user does not exist, create it.
		User facebookUser = repository.findByFacebookId(user.getFacebookId());
		if( facebookUser == null ) {
			facebookUser = createFacebookUser(facebookId,facebookToken);
		}
		
		// Generate a new token for the user.
		MittoToken mittoToken = new MittoToken();
		mittoToken.setToken( generateToken() );
		mittoToken.setUser(facebookUser);
		facebookUser.getMittoTokens().add(mittoToken);
		
		repository.save(facebookUser);
		return mittoToken;
	}
	
	private String generateToken() {
		return UUID.randomUUID().toString();
	}

	
	/**
	 * Checks if the user exists based on the profile found from the Facebook token.
	 * If the Facebook id given matches the Facebook id on the found profile, the user exists.
	 * @param user The user in question.
	 * @return True if the user exists, false if not.
	 */
	private boolean facebookUserExists( User user, String facebookToken ) {
		Facebook facebook = new FacebookTemplate(facebookToken);
		String lookedUpFacebookId = facebook.userOperations().getUserProfile().getId();
		return lookedUpFacebookId.equals(user.getFacebookId());
	}
}
