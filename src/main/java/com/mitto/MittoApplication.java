package com.mitto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitto.config.HibernateAwareObjectMapper;

@SpringBootApplication
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class MittoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MittoApplication.class, args);
	}

	@Bean
	public ObjectMapper provideAwareObjectMapper() {
		return new HibernateAwareObjectMapper();
	}
}
