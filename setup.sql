CREATE TABLE user (
	id int(11) unsigned auto_increment,
	facebook_id varchar(255) not null unique,
	display_name varchar(255) not null,
	facebook_token varchar(255) not null unique,
	fcm_notification_token varchar(255),
	PRIMARY KEY( id )
) ENGINE=InnoDB;

CREATE TABLE mitto_token (
	id int(12) unsigned auto_increment,
	user_id int(11) unsigned,
	token varchar(255),
	primary key( id ),
	foreign key( user_id ) references user(id)
) ENGINE=InnoDB;

create table fcm (
	id int(11) unsigned not null auto_increment,
	token varchar(255) default null,
	user_id int(11) unsigned not null,
	primary key( id ),
	foreign key( user_id ) references User(id)
) engine=InnoDB;

create table iou (
	id int(11) unsigned not null auto_increment,
	transaction_type int(11) not null,
	amount double not null,
	friend_involved int(11) unsigned not null,
	creator int(11) unsigned not null,
	memo varchar(255) default "",
	current_status varchar(255) not null,
	date_created datetime not null,
	date_modified datetime not null,
	primary key( id ),
	foreign key( friend_involved ) references User(id),
	foreign key( creator ) references User(id)
) engine=InnoDB;

create table action (
	id int(11) unsigned not null auto_increment,
	actor int(11) unsigned not null,
	parent_id int(11) unsigned not null,
	parent_type int(11) unsigned not null,
	action_type tinyint unsigned not null,
	primary key( id )
) engine=InnoDB;

create table notification (
	id int(11) unsigned not null auto_increment,
	action_id int(11) unsigned not null,
	date_created datetime not null,
	user_id int(11) unsigned not null,
	status varchar(255) not null,
	date_created datetime not null,
	date_modified datetime not null,
	foreign key( user_id ) references user(id),
	foreign key( action_id ) references action(id),
	primary key( id )
) engine=InnoDB;


CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `ious_created` AS
    SELECT 
        SUM((CASE
            WHEN (`iou`.`transaction_type` = 0) THEN -(`iou`.`amount`)
            ELSE `iou`.`amount`
        END)) AS `owed_to_you`,
        `iou`.`friend_involved` AS `friend`
    FROM
        `iou`
    GROUP BY `friend`;
    
    
CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `ious_involved` AS
    SELECT 
        SUM((CASE
            WHEN (`iou`.`transaction_type` = 1) THEN -(`iou`.`amount`)
            ELSE `iou`.`amount`
        END)) AS `owed_to_you`,
        `iou`.`creator` AS `friend`
    FROM
        `iou`
    GROUP BY `iou`.`creator`;



